<?php

namespace TheCodingMachine\Gotenberg;

use function count;

abstract class ChromeRequest extends Request implements GotenbergRequestInterface
{
    const WAIT_DELAY = 'waitDelay';
    const PAPER_WIDTH = 'paperWidth';
    const PAPER_HEIGHT = 'paperHeight';
    const MARGIN_TOP = 'marginTop';
    const MARGIN_BOTTOM  = 'marginBottom';
    const MARGIN_LEFT = 'marginLeft';
    const MARGIN_RIGHT = 'marginRight';
    const LANDSCAPE = 'landscape';
    const PAGE_RANGES = 'pageRanges';
    const GOOGLE_CHROME_RPCC_BUFFER_SIZE = 'googleChromeRpccBufferSize';
    const SCALE = 'scale';

    /** @var float|null */
    private $waitDelay;

    /** @var Document|null */
    private $header;

    /** @var Document|null */
    private $footer;

    /** @var float|null */
    private $paperWidth;

    /** @var float|null */
    private $paperHeight;

    /** @var float|null */
    private $marginTop;

    /** @var float|null */
    private $marginBottom;

    /** @var float|null */
    private $marginLeft;

    /** @var float|null */
    private $marginRight;

    /** @var bool */
    private $landscape;

    /** @var string|null */
    private $pageRanges;

    /** @var int|null */
    private $googleChromeRpccBufferSize;

    /** @var float|null */
    private $scale;

    /**
     * @return array<string,mixed>
     */
    public function getFormValues()
    {
        $values = parent::getFormValues();
        if ($this->waitDelay !== null) {
            $values[self::WAIT_DELAY] = $this->waitDelay;
        }
        if ($this->paperWidth !== null) {
            $values[self::PAPER_WIDTH] = $this->paperWidth;
        }
        if ($this->paperHeight !== null) {
            $values[self::PAPER_HEIGHT] = $this->paperHeight;
        }
        if ($this->marginTop !== null) {
            $values[self::MARGIN_TOP] = $this->marginTop;
        }
        if ($this->marginBottom !== null) {
            $values[self::MARGIN_BOTTOM] = $this->marginBottom;
        }
        if ($this->marginLeft !== null) {
            $values[self::MARGIN_LEFT] = $this->marginLeft;
        }
        if ($this->marginRight !== null) {
            $values[self::MARGIN_RIGHT] = $this->marginRight;
        }
        if ($this->pageRanges !== null) {
            $values[self::PAGE_RANGES] = $this->pageRanges;
        }
        if ($this->googleChromeRpccBufferSize !== null) {
            $values[self::GOOGLE_CHROME_RPCC_BUFFER_SIZE] = $this->googleChromeRpccBufferSize;
        }
        if ($this->scale !== null) {
            $values[self::SCALE] = $this->scale;
        }
        $values[self::LANDSCAPE] = $this->landscape;

        return $values;
    }

    /**
     * @return array<string,Document>
     */
    public function getFormFiles()
    {
        $files = [];
        if (! empty($this->header)) {
            $files['header.html'] = $this->header;
        }
        if (! empty($this->footer)) {
            $files['footer.html'] = $this->footer;
        }

        return $files;
    }

    /**
     * @param null|float $waitDelay
     */
    public function setWaitDelay($waitDelay)
    {
        $this->waitDelay = $waitDelay;
    }

    /**
     * @param float[] $paperSize
     *
     * @throws RequestException
     */
    public function setPaperSize(array $paperSize)
    {
        if (count($paperSize) !== 2) {
            throw new RequestException('Wrong paper size.');
        }
        $this->paperWidth = $paperSize[0];
        $this->paperHeight = $paperSize[1];
    }

    /**
     * @param float[] $margins
     *
     * @throws RequestException
     */
    public function setMargins(array $margins)
    {
        if (count($margins) !== 4) {
            throw new RequestException('Wrong margins.');
        }
        $this->marginTop = $margins[0];
        $this->marginBottom = $margins[1];
        $this->marginLeft = $margins[2];
        $this->marginRight = $margins[3];
    }

    /**
     * @param null|Document $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @param null|Document $footer
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;
    }

    /**
     * @param null|float $paperWidth
     */
    public function setPaperWidth($paperWidth)
    {
        $this->paperWidth = $paperWidth;
    }

    /**
     * @param null|float $paperHeight
     */
    public function setPaperHeight($paperHeight)
    {
        $this->paperHeight = $paperHeight;
    }

    /**
     * @param null|float $marginTop
     */
    public function setMarginTop($marginTop)
    {
        $this->marginTop = $marginTop;
    }

    /**
     * @param null|float $marginBottom
     */
    public function setMarginBottom($marginBottom)
    {
        $this->marginBottom = $marginBottom;
    }

    /**
     * @param null|float $marginLeft
     */
    public function setMarginLeft($marginLeft)
    {
        $this->marginLeft = $marginLeft;
    }

    /**
     * @param null|float $marginRight
     */
    public function setMarginRight($marginRight)
    {
        $this->marginRight = $marginRight;
    }

    /**
     * @param bool $landscape
     */
    public function setLandscape($landscape)
    {
        $this->landscape = $landscape;
    }

    /**
     * @param string $pageRanges
     */
    public function setPageRanges($pageRanges)
    {
        $this->pageRanges = $pageRanges;
    }

    /**
     * @param null|int $googleChromeRpccBufferSize
     */
    public function setGoogleChromeRpccBufferSize($googleChromeRpccBufferSize)
    {
        $this->googleChromeRpccBufferSize = $googleChromeRpccBufferSize;
    }

    /**
     * @param null|float $scale
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    }
}
