<?php

namespace TheCodingMachine\Gotenberg;

abstract class Request
{
    const A3 = [11.7, 16.5];
    const A4 = [8.27, 11.7];
    const A5 = [5.8, 8.2];
    const A6 = [4.1, 5.8];
    const LETTER = [8.5, 11];
    const LEGAL = [8.5, 14];
    const TABLOID = [11, 17];

    const NO_MARGINS = [0, 0, 0, 0];
    const NORMAL_MARGINS = [1, 1, 1, 1];
    const LARGE_MARGINS = [2, 2, 2, 2];

    const RESULT_FILENAME = 'resultFilename';
    const WAIT_TIMEOUT = 'waitTimeout';
    const WEBHOOK_URL = 'webhookURL';
    const WEBHOOK_URL_TIMEOUT = 'webhookURLTimeout';

    const WEBHOOK_URL_BASE_HTTP_HEADER_KEY = 'Gotenberg-Webhookurl-';

    /** @var string|null */
    private $resultFilename;

    /** @var float|null */
    private $waitTimeout;

    /** @var string|null */
    private $webhookURL;

    /** @var float|null */
    private $webhookURLTimeout;

    /** @var array<string,string> */
    protected $customHTTPHeaders;

    public function __construct()
    {
        $this->customHTTPHeaders = [];
    }

    /**
     * @return array<string,mixed>
     */
    public function getFormValues()
    {
        $values = [];
        if (! empty($this->resultFilename)) {
            $values[self::RESULT_FILENAME] = $this->resultFilename;
        }
        if ($this->waitTimeout !== null) {
            $values[self::WAIT_TIMEOUT] = $this->waitTimeout;
        }
        if (! empty($this->webhookURL)) {
            $values[self::WEBHOOK_URL] = $this->webhookURL;
        }
        if (! empty($this->webhookURLTimeout)) {
            $values[self::WEBHOOK_URL_TIMEOUT] = $this->webhookURLTimeout;
        }

        return $values;
    }

    /**
     * @return bool
     */
    public function hasWebhook()
    {
        return ! empty($this->webhookURL);
    }

    /**
     * @return array<string,string>
     */
    public function getCustomHTTPHeaders()
    {
        return $this->customHTTPHeaders;
    }

    /**
     * @param null|string $resultFilename
     */
    public function setResultFilename($resultFilename)
    {
        $this->resultFilename = $resultFilename;
    }

    /**
     * @param null|float $waitTimeout
     */
    public function setWaitTimeout($waitTimeout)
    {
        $this->waitTimeout = $waitTimeout;
    }

    /**
     * @param null|string $webhookURL
     */
    public function setWebhookURL($webhookURL)
    {
        $this->webhookURL = $webhookURL;
    }

    /**
     * @param null|float $webhookURLTimeout
     */
    public function setWebhookURLTimeout($webhookURLTimeout)
    {
        $this->webhookURLTimeout = $webhookURLTimeout;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addWebhookURLHTTPHeader($key, $value)
    {
        $key = self::WEBHOOK_URL_BASE_HTTP_HEADER_KEY . $key;
        $this->customHTTPHeaders[$key] = $value;
    }
}
