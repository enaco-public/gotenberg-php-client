<?php

namespace TheCodingMachine\Gotenberg;

use Exception;

final class RequestException extends Exception
{
}
