<?php

namespace TheCodingMachine\Gotenberg;

use GuzzleHttp\Psr7\LazyOpenStream;
use Psr\Http\Message\StreamInterface;
use function fopen;
use function fwrite;
use function GuzzleHttp\Psr7\stream_for;

final class DocumentFactory
{
    /**
     * @param string $fileName
     * @param string $filePath
     *
     * @return Document
     */
    public static function makeFromPath($fileName, $filePath)
    {
        return new Document($fileName, new LazyOpenStream($filePath, 'r'));
    }

    /**
     * @param string          $fileName
     * @param StreamInterface $fileStream
     *
     * @return Document
     */
    public static function makeFromStream($fileName, StreamInterface $fileStream)
    {
        return new Document($fileName, $fileStream);
    }

    /**
     * @param string $fileName
     * @param string $string
     *
     * @return Document
     */
    public static function makeFromString($fileName, $string)
    {
        $fileStream = fopen('php://memory', 'rb+');
        fwrite($fileStream, $string);

        return new Document($fileName, stream_for($fileStream));
    }
}
