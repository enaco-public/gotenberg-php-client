<?php

namespace TheCodingMachine\Gotenberg;

final class OfficeRequest extends Request implements GotenbergRequestInterface
{
    const LANDSCAPE = 'landscape';
    const PAGE_RANGES = 'pageRanges';

    /** @var Document[] */
    private $files;

    /** @var bool */
    private $landscape;

    /** @var string|null */
    private $pageRanges;

    /**
     * @param Document[] $files
     */
    public function __construct(array $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getPostURL()
    {
        return '/convert/office';
    }

    /**
     * @return array<string,mixed>
     */
    public function getFormValues()
    {
        $values = parent::getFormValues();
        if ($this->pageRanges !== null) {
            $values[self::PAGE_RANGES] = $this->pageRanges;
        }
        $values[self::LANDSCAPE] = $this->landscape;

        return $values;
    }

    /**
     * @return array<string,Document>
     */
    public function getFormFiles()
    {
        $files = [];
        foreach ($this->files as $file) {
            $files[$file->getFileName()] = $file;
        }

        return $files;
    }

    /**
     * @param bool $landscape
     */
    public function setLandscape($landscape)
    {
        $this->landscape = $landscape;
    }

    /**
     * @param string $pageRanges
     */
    public function setPageRanges($pageRanges)
    {
        $this->pageRanges = $pageRanges;
    }
}
