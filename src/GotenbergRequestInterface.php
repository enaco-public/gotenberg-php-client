<?php

namespace TheCodingMachine\Gotenberg;

interface GotenbergRequestInterface
{
    /**
     * @return string
     */
    public function getPostURL();

    /**
     * @return array<string,string>
     */
    public function getCustomHTTPHeaders();

    /**
     * @return array<string,mixed>
     */
    public function getFormValues();

    /**
     * @return array<string,Document>
     */
    public function getFormFiles();

    /**
     * @return bool
     */
    public function hasWebhook();
}
