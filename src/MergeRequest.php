<?php

namespace TheCodingMachine\Gotenberg;

final class MergeRequest extends Request implements GotenbergRequestInterface
{
    /** @var Document[] */
    private $files;

    /**
     * @param Document[] $files
     */
    public function __construct(array $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getPostURL()
    {
        return '/merge';
    }

    /**
     * @return array<string,Document>
     */
    public function getFormFiles()
    {
        $files = [];
        foreach ($this->files as $file) {
            $files[$file->getFileName()] = $file;
        }

        return $files;
    }
}
