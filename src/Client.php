<?php

namespace TheCodingMachine\Gotenberg;

use Exception;
use GuzzleHttp\Psr7\MultipartStream;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use function fclose;
use function fopen;
use function fwrite;

final class Client
{
    /** @var HttpClient */
    private $client;

    /** @var string */
    private $apiURL;

    /**
     * Client constructor.
     *
     * @param string          $apiURL
     * @param null|HttpClient $client
     */
    public function __construct($apiURL, $client = null)
    {
        $this->apiURL = $apiURL;
        $this->client = $client ?: HttpClientDiscovery::find();
    }

    /**
     * Sends the given documents to the API and returns the response.
     *
     * @throws ClientException
     * @throws Exception
     */
    public function post(GotenbergRequestInterface $request)
    {
        return $this->handleResponse($this->client->sendRequest($this->makeMultipartFormDataRequest($request)));
    }

    /**
     * Sends the given documents to the API, stores the resulting PDF in the given destination.
     *
     * @param GotenbergRequestInterface $request
     * @param                           $destination
     *
     * @throws ClientException
     * @throws RequestException
     * @throws \Http\Client\Exception
     */
    public function store(GotenbergRequestInterface $request, $destination)
    {
        if ($request->hasWebhook()) {
            throw new RequestException('Cannot use method store with a webhook.');
        }
        $response = $this->handleResponse($this->client->sendRequest($this->makeMultipartFormDataRequest($request)));
        $fileStream = $response->getBody();
        $fp = fopen($destination, 'w');
        fwrite($fp, $fileStream->getContents());
        fclose($fp);
    }

    /**
     * @param GotenbergRequestInterface $request
     *
     * @return RequestInterface
     */
    private function makeMultipartFormDataRequest(GotenbergRequestInterface $request)
    {
        $multipartData = [];
        foreach ($request->getFormValues() as $fieldName => $fieldValue) {
            $multipartData[] = [
                'name' => $fieldName,
                'contents' => $fieldValue,
            ];
        }
        /**
         * @var string $filename
         * @var Document $document
         */
        foreach ($request->getFormFiles() as $filename => $document) {
            $multipartData[] = [
                'name' => 'files',
                'filename' => $filename,
                'contents' => $document->getFileStream(),
            ];
        }
        $body = new MultipartStream($multipartData);
        $messageFactory = MessageFactoryDiscovery::find();
        $message = $messageFactory
            ->createRequest('POST', $this->apiURL . $request->getPostURL())
            ->withHeader('Content-Type', 'multipart/form-data; boundary="' . $body->getBoundary() . '"')
            ->withBody($body);
        foreach ($request->getCustomHTTPHeaders() as $key => $value) {
            $message = $message->withHeader($key, $value);
        }

        return $message;
    }

    /**
     * @return ResponseInterface
     * @throws ClientException
     */
    private function handleResponse(ResponseInterface $response)
    {
        switch ($response->getStatusCode()) {
            case 200:
                return $response;
            default:
                throw new ClientException($response->getBody()->getContents(), $response->getStatusCode());
        }
    }
}
