<?php

namespace TheCodingMachine\Gotenberg;

final class URLRequest extends ChromeRequest implements GotenbergRequestInterface
{
    const REMOTE_URL = 'remoteURL';

    const REMOTE_URL_BASE_HTTP_HEADER_KEY = 'Gotenberg-Remoteurl-';

    /** @var string */
    private $URL;

    /**
     * URLRequest constructor.
     *
     * @param string $URL
     */
    public function __construct($URL)
    {
        parent::__construct();
        $this->URL = $URL;
    }

    /**
     * @return string
     */
    public function getPostURL()
    {
        return '/convert/url';
    }

    /**
     * @return array<string,mixed>
     */
    public function getFormValues()
    {
        $values = parent::getFormValues();
        $values[self::REMOTE_URL] = $this->URL;

        return $values;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addRemoteURLHTTPHeader($key, $value)
    {
        $key = self::REMOTE_URL_BASE_HTTP_HEADER_KEY . $key;
        $this->customHTTPHeaders[$key] = $value;
    }
}
