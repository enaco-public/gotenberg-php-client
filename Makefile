# start workspace.
workspace:
	docker-compose run --rm php bash

# run all tests.
test:
	docker-compose up -d gotenberg
	docker-compose run --rm -T php composer run phpunit
	docker-compose down